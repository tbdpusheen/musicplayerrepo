package com.example.musicplayerproject;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;

import static java.lang.Thread.sleep;

@SuppressWarnings({"SameParameterValue", "IntegerDivisionInFloatingPointContext"})
public class MainActivity extends AppCompatActivity implements Runnable {

    //region Properties
    final Playlist playlist = new Playlist();
    final String artistLilyPichu = "LilyPichu";
    final ArrayList<Drawable> topBarImages = new ArrayList<>();
    Song songInitiated;
    SongAdapter songAdapter;
    boolean loopingSong;
    boolean isPaused;
    int currentlyPlayingIndex = 0;
    int currentTopBarImageIndex = 0;
    MediaPlayer mediaPlayer = null;

    // XML Views
    ImageView topBarImage;
    TextView playlistName;
    ImageButton shuffleButton;
    ImageButton loopButton;
    RecyclerView songsRecyclerView;
    ImageView currentSongThumbnail;
    TextView currentSongName;
    TextView currentSongArtist;
    TextView currentSongDuration;
    TextView currentSongPosition;
    SeekBar seekBar;
    ImageButton previousSongButton;
    ImageButton playButton;
    ImageButton pauseButton;
    ImageButton skipButton;
    ImageButton changeTopBarButton;
    ColorStateList turquoiseColor;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        System.out.println("App Started");
        connectXMLViews();

        // Initialize properties of the playlist
        playlist.name = "comfi beats";
        playlist.songs = new ArrayList<>();
        System.out.println("Playlist created");

        // Initializes the top bar images
        topBarImages.add(ContextCompat.getDrawable(this, R.drawable.top_bar_background_1));
        topBarImages.add(ContextCompat.getDrawable(this, R.drawable.top_bar_background_2));
        topBarImages.add(ContextCompat.getDrawable(this, R.drawable.top_bar_background_3));
        topBarImage.setImageDrawable(topBarImages.get(currentTopBarImageIndex));
        System.out.println("Top bar images added");

        populateSongsPlaylist();
        setupRecyclerView();
        currentSong();
        setupButtonHandlers();

        // Seeks to the selected distance on the seek bar
        seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.seekTo(seekBar.getProgress());
                    System.out.println("Song position seeked to " + seekBar.getProgress() + " milliseconds");
                }
                else if (mediaPlayer != null) {
                    seekBar.setProgress(mediaPlayer.getCurrentPosition());
                    System.out.println("Error: Tried to change seekBar position while song was paused");
                }
            }
        });
        System.out.println("seekBar initiated");
    }

    // Populates the data model (playlist.songs) with the given information
    void populateDataModel(String songName, String artistName, int songDurationSeconds, int thumbnailResource, int mp3Resource) {
        // Create a new song using the input song parameters
        songInitiated = new Song();
        songInitiated.songName = songName;
        songInitiated.artistName = artistName;
        songInitiated.thumbnailResource = thumbnailResource;
        songInitiated.mp3Resource = mp3Resource;

        // If the song has a duration of x:0x, add the zero after the colon
        if (songDurationSeconds % 60 < 10) {
            songInitiated.songDuration = songDurationSeconds / 60 + ":0" + songDurationSeconds % 60;
        }
        else {
            songInitiated.songDuration = songDurationSeconds / 60 + ":" + songDurationSeconds % 60;
        }

        // Add the newly created song to the playlist
        playlist.songs.add(songInitiated);

        System.out.println(songName + " added to data model");
    }

    // Populates the playlist with songs
    void populateSongsPlaylist() {
        populateDataModel("comfy vibes", artistLilyPichu, 194, R.drawable.comfy_vibes_thumbnail_2, R.raw.comfy_vibes);
        populateDataModel("dreamy nightmares", artistLilyPichu, 197, R.drawable.dreamy_nightmares_thumbnail_2, R.raw.dreamy_nightmares);
        populateDataModel("foreverland", artistLilyPichu, 146, R.drawable.foreverland_thumbnail_2, R.raw.foreverland);
        populateDataModel("sunshines & butterflies", artistLilyPichu, 185, R.drawable.sunshine_and_butterflies_thumbnail_2, R.raw.sunshine_and_butterflies);
        populateDataModel("wilting memories", artistLilyPichu, 108, R.drawable.wilting_memories_thumbnail_2, R.raw.wilting_memories);
        populateDataModel("a vision", artistLilyPichu, 169, R.drawable.a_vision_thumbnail_2, R.raw.a_vision);
        populateDataModel("unknown waters", artistLilyPichu, 116, R.drawable.unknown_waters_thumbnail_2, R.raw.unknown_waters);
        populateDataModel("a stormy night", artistLilyPichu, 202, R.drawable.a_stormy_night_thumbnail_2, R.raw.a_stormy_night);
        populateDataModel("these days it's hard to find the words", artistLilyPichu, 168, R.drawable.these_days_its_hard_to_find_the_words_thumbnail_2, R.raw.these_days_its_hard_to_find_the_words);
        populateDataModel("dreamy night", artistLilyPichu, 246, R.drawable.dreamy_night_thumbnail_2, R.raw.dreamy_night);
        populateDataModel("if there was a zombie apocalypse i'd let my dog eat me", artistLilyPichu, 225, R.drawable.if_there_was_a_zombie_apocalypse_id_let_my_dog_eat_me_thumbnail_2, R.raw.if_there_was_a_zombie_apocalypse_id_let_my_dog_eat_me);

        System.out.println("All songs added. Playlist is as follows: ");
        for (int song = 0; song < playlist.songs.size(); song++) {
            System.out.println(playlist.songs.get(song).songName);
        }

        playlistName.setText(playlist.name);
    }

    // Connects the XML views into java
    void connectXMLViews() {
        topBarImage = findViewById(R.id.top_bar_background);
        playlistName = findViewById(R.id.playlist_name);
        shuffleButton = findViewById(R.id.shuffle_button);
        loopButton = findViewById(R.id.loop_button);
        changeTopBarButton = findViewById(R.id.change_top_bar_button);
        songsRecyclerView = findViewById(R.id.song_list_recyclerView);
        currentSongThumbnail = findViewById(R.id.current_song_thumbnail);
        currentSongName = findViewById(R.id.current_song_name);
        currentSongArtist = findViewById(R.id.current_artist_name);
        currentSongDuration = findViewById(R.id.song_duration_end);
        previousSongButton = findViewById(R.id.previous_song_button);
        playButton = findViewById(R.id.play_button);
        pauseButton = findViewById(R.id.pause_button);
        skipButton = findViewById(R.id.next_song_button);
        seekBar = findViewById(R.id.seekBar);
        turquoiseColor = getColorStateList(R.color.turquoise);

        System.out.println("XML views connected");
    }

    // Sets the current song bar to the current song
    void currentSong() {
        Song currentSong = playlist.songs.get(currentlyPlayingIndex);
        currentSongThumbnail.setImageResource(currentSong.thumbnailResource);
        currentSongName.setText(currentSong.songName);
        currentSongName.setHorizontallyScrolling(currentSong.songName.length() > 15);
        currentSongArtist.setText(currentSong.artistName);
        currentSongDuration.setText(currentSong.songDuration);

        System.out.println("Set the current song to index " + currentlyPlayingIndex);
    }

    // Sets up onClickListeners
    void setupButtonHandlers() {
        previousSongButton.setOnClickListener(v -> {
            System.out.println("Previous button tapped.");

            // If the index is not out of bounds, decrement the index by 1
            if (currentlyPlayingIndex - 1 >= 0 ) {
                switchSong(currentlyPlayingIndex, currentlyPlayingIndex - 1);
            }
        });

        skipButton.setOnClickListener(v -> {
            System.out.println("Skip button tapped.");
            switchSong(currentlyPlayingIndex, currentlyPlayingIndex + 1);
        });

        playButton.setOnClickListener(v -> {
            System.out.println("Play button tapped.");
            playCurrentSong();
        });

        pauseButton.setOnClickListener(v -> {
            System.out.println("Pause button tapped.");
            pauseCurrentSong();
        });

        loopButton.setOnClickListener(v -> {
            System.out.println("Loop button tapped.");
            loopCurrentSong();
        });

        shuffleButton.setOnClickListener(v -> {
            System.out.println("Shuffle button tapped.");
            shufflePlaylist();
        });

        changeTopBarButton.setOnClickListener(v -> {
            System.out.println("Change top bar button tapped.");
            changeTopBarImage();
        });
    }

    // Switches the current song to the selected index
    void switchSong(int fromIndex, int toIndex) {

        // If the index is out of bounds, loop it back to the first song in the playlist
        if (toIndex >= playlist.songs.size()) {
            toIndex = 0;
        }

        // Switch the current song index into the index selected, and scroll to it
        currentlyPlayingIndex = toIndex;
        songAdapter.notifyItemChanged(fromIndex);
        songAdapter.notifyItemChanged(currentlyPlayingIndex);
        songsRecyclerView.scrollToPosition(currentlyPlayingIndex);

        currentSong();

        // Play the song selected
        if (mediaPlayer != null) {
            mediaPlayer.release();
        }
        mediaPlayer = null;
        playCurrentSong();

        // If looping was previously enabled, enable it again
        if (loopingSong) {
            loopCurrentSong();
        }

        System.out.println("Song switched from index " + fromIndex + " to index " + toIndex + " (" + playlist.songs.get(toIndex).songName + ")");
    }

    // Plays the song at the current index (currentSongIndex)
    void playCurrentSong() {
        if (mediaPlayer == null) {
            // If the media player doesn't exist yet, create one that plays the current song
            Song currentSong = playlist.songs.get(currentlyPlayingIndex);
            mediaPlayer = MediaPlayer.create(MainActivity.this, currentSong.mp3Resource);

            // When the song finishes playing, play the next song in the index if it is not looping
            mediaPlayer.setOnCompletionListener(mp -> {
                if (!mediaPlayer.isLooping()) {
                    switchSong(currentlyPlayingIndex, currentlyPlayingIndex + 1);
                }
            });

            seekBar.setMax(mediaPlayer.getDuration());
            System.out.println("New mediaPlayer created");
        }

        // Starts the media player and switches to the pause button
        mediaPlayer.start();
        isPaused = false;
        playButton.setVisibility(View.INVISIBLE);
        pauseButton.setVisibility(View.VISIBLE);

        System.out.println("Playing current song (" + currentSongName.getText() + ")");
        new Thread(this).start();
    }

    // Pauses the media player
    void pauseCurrentSong() {
        if (mediaPlayer != null) {
            mediaPlayer.pause();
            isPaused = true;
        }

        // Switches to the play button
        pauseButton.setVisibility(View.INVISIBLE);
        playButton.setVisibility(View.VISIBLE);

        System.out.println("Pausing song");
    }

    // Activates looping
    void loopCurrentSong() {
        // If the mediaPlayer is not currently looping, enable looping
        if (mediaPlayer != null && !mediaPlayer.isLooping()) {
            mediaPlayer.setLooping(true);
            loopButton.setImageTintList(turquoiseColor);

            loopingSong = true;
            System.out.println("Looping enabled");
        }
        // If looping was already enabled, disable it
        else if (mediaPlayer != null) {
            mediaPlayer.setLooping(false);
            loopButton.setImageTintList(null);

            loopingSong = false;
            System.out.println("Looping disabled");
        }
    }

    // Shuffles the playlist.songs and resets the recyclerView
    void shufflePlaylist() {
        Collections.shuffle(playlist.songs);

        // Sets the currentlyPlayingIndex to the start of the newly shuffled playlist
        if (mediaPlayer != null) {
            currentlyPlayingIndex = -1;
        }
        else {
            currentSong();
        }

        songAdapter.notifyDataSetChanged();
        songsRecyclerView.scrollToPosition(0);

        System.out.println("Playlist shuffled");
    }

    // Sets up the recyclerView
    void setupRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        songsRecyclerView.setLayoutManager(layoutManager);

        // Connect the adapter to the recyclerView
        songAdapter = new SongAdapter(this, playlist.songs, this);
        songsRecyclerView.setAdapter(songAdapter);

        System.out.println("RecyclerView and SongAdapter connected");
    }

    // Switches the top bar image
    void changeTopBarImage() {
        // Sets the top bar image to the next image in the index. If the next image is out of bounds, set the image to the first image.
        if (currentTopBarImageIndex + 1 < topBarImages.size()) {
            currentTopBarImageIndex ++;
        }
        else {
            currentTopBarImageIndex = 0;
        }

        topBarImage.setImageDrawable(topBarImages.get(currentTopBarImageIndex));
        System.out.println("Set top bar image to the image at index " + currentTopBarImageIndex);
    }

    // When the user selects a song in the recyclerView, switch to that song
    void onUserSelectedSongAtPosition(int position) {
        if (currentlyPlayingIndex != position) {
            switchSong(currentlyPlayingIndex, position);
        }
    }

    // Implementation of Runnable
    public void run() {

        int currentPosition = mediaPlayer.getCurrentPosition();
        int total = mediaPlayer.getDuration();

        while (mediaPlayer != null && !isPaused && currentPosition < total) {
            try {
                //noinspection BusyWait
                sleep(100);
                currentPosition = mediaPlayer.getCurrentPosition();
            } catch (Exception e) {
                return;
            }

            seekBar.setProgress(currentPosition);

            runOnUiThread(() -> {
                currentSongPosition = findViewById(R.id.current_song_position);
                int songPositionSeconds = (int)Math.ceil(mediaPlayer.getCurrentPosition() / 1000f);

                String currentPositionTime;
                if (songPositionSeconds % 60 < 10) {
                    currentPositionTime = (int)Math.ceil(songPositionSeconds / 60) + ":0" + songPositionSeconds % 60;
                } else {
                    currentPositionTime = (int)Math.ceil(songPositionSeconds / 60) + ":" + songPositionSeconds % 60;
                }
                currentSongPosition.setText(currentPositionTime);
            });
        }
    }

    // Stops the music player if the app is stopped
    @Override
    protected void onStop() {
        super.onStop();
        mediaPlayer.release();
        mediaPlayer = null;
    }
}
