package com.example.musicplayerproject;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SongViewHolder extends RecyclerView.ViewHolder {

    public SongViewHolder(@NonNull View itemView) {
        super(itemView);
        thumbnailImageView = itemView.findViewById(R.id.song_thumbnail);
        songNameTextView = itemView.findViewById(R.id.song_name);
        songArtistTextView = itemView.findViewById(R.id.artist_name);
        currentlyPlayingImageView = itemView.findViewById(R.id.currently_playing);
        songDurationTextView = itemView.findViewById(R.id.song_duration);
        emptyHeartImageButton = itemView.findViewById(R.id.empty_heart_button);
        filledHeartImageButton = itemView.findViewById(R.id.filled_heart_button);
    }

    final ImageView thumbnailImageView;
    final TextView songNameTextView;
    final TextView songArtistTextView;
    final ImageView currentlyPlayingImageView;
    final TextView songDurationTextView;
    final ImageButton emptyHeartImageButton;
    final ImageButton filledHeartImageButton;
}
