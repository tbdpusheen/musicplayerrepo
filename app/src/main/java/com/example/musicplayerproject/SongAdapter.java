package com.example.musicplayerproject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class SongAdapter extends RecyclerView.Adapter<SongViewHolder> {

    SongAdapter(@NonNull Context context, @NonNull ArrayList<Song> songs, @NonNull MainActivity mainActivity) {
        this.context = context;
        this.songs = songs;
        this.mainActivity = mainActivity;
    }

    // Inflates the viewHolder
    @NonNull
    @Override
    public SongViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_song, parent, false);
        return new SongViewHolder(itemView);
    }

    // When the viewHolder is created
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull SongViewHolder holder, int position) {
        // Create the viewHolder using the parameters of the song at the specified index
        Song song = songs.get(position);
        holder.thumbnailImageView.setImageResource(song.thumbnailResource);
        holder.songNameTextView.setText(song.songName);
        holder.songArtistTextView.setText(song.artistName);
        holder.songDurationTextView.setText(song.songDuration);
        System.out.println("Initialized song parameters at index " + position);

        // Replaces the end of the song name with "..." if the song name is too long\
        if (holder.songNameTextView.length() > 17) {
            holder.songNameTextView.setText(song.songName.substring(0, 17) + "...");
            System.out.println("Song name string shortened at index " + position);
        }

        // If the song generated is the song at the current index, show the currently playing image
        if (position == mainActivity.currentlyPlayingIndex) {
            holder.currentlyPlayingImageView.setVisibility(View.VISIBLE);
        }
        else {
            holder.currentlyPlayingImageView.setVisibility(View.INVISIBLE);
        }

        // If the song generated was liked, then make it liked
        if (song.isLiked) {
            holder.emptyHeartImageButton.setVisibility(View.INVISIBLE);
            holder.filledHeartImageButton.setVisibility(View.VISIBLE);
        }
        else {
            holder.emptyHeartImageButton.setVisibility(View.VISIBLE);
            holder.filledHeartImageButton.setVisibility(View.INVISIBLE);
        }

        // When the user selects the song, switch to that song
        holder.itemView.setOnClickListener(v -> {
            System.out.println("User tapped on song at index " + position);
            mainActivity.onUserSelectedSongAtPosition(position);
        });

        // WHen the user clicks on an empty heart, like the song
        holder.emptyHeartImageButton.setOnClickListener(v -> {
            songs.get(position).isLiked = true;
            holder.emptyHeartImageButton.setVisibility(View.INVISIBLE);
            holder.filledHeartImageButton.setVisibility(View.VISIBLE);
            System.out.println("Empty heart button pressed");
        });

        // WHen the user clicks on a filled heart, dislike the song
        holder.filledHeartImageButton.setOnClickListener(v -> {
            songs.get(position).isLiked = false;
            holder.filledHeartImageButton.setVisibility(View.INVISIBLE);
            holder.emptyHeartImageButton.setVisibility(View.VISIBLE);
            System.out.println("Filled heart button pressed");
        });
    }

    @Override
    public int getItemCount() {
        return songs.size();
    }

    // Properties
    final Context context;
    final ArrayList<Song> songs;
    final MainActivity mainActivity;
}
