package com.example.musicplayerproject;

public class Song {

    String songName;
    String artistName;
    String songDuration;
    int thumbnailResource;
    int mp3Resource;
    boolean isLiked = false;
}
