## How to use this repository

--------------------------------------------

Clone this repository by pressing the *clone* button on the top right. If you're using *SSH*, change the dropdown from HTTPS to SSH to get the SSH link.

Follow the GOA tutorial video on the workflow of using git or look at [this link](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow) to see how to commit and push to this repository.

Ask Bastien if you ever need any help. Ever in quotation marks.